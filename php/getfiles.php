<?php
require_once('globals.php');
$relativeFileLocation = "../".FILES_LOCATION;

$files = scandir($relativeFileLocation);
$filesArray = array();

foreach ($files as $file) {
    if ($file != '.' && $file != '..') {
        $fileArray = array(
            'name' => $file,
            'url' => FILES_LOCATION.'/'.rawurlencode($file),
            "modified" => date('m/d/Y', stat($relativeFileLocation.'/'.$file)['mtime']),
            "size" => stat($relativeFileLocation.'/'.$file)['size']
        );
        array_push($filesArray, $fileArray);
    }
}
echo json_encode($filesArray);
?>
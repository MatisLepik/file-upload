<?php
require_once('globals.php');

if (password_verify($_POST['pw'], HASHED_PW)) {
    uploadFile();
} 
else {
    echo 'Wrongpass';
}

function uploadFile() {
    $targetFile = '../'.FILES_LOCATION.'/'.$_FILES['file']['name'];
    
    if ( $_FILES['file']['error'] > 0) {
        echo 'Error';
    } 
    else {
        
        if (file_exists($targetFile)) {
            $originalFile = '../'.FILES_LOCATION.'/'. $_FILES['file']['name'];
            $originalExt = pathinfo($originalFile, PATHINFO_EXTENSION);
            $originalNoExt = substr($originalFile, 0, -1-strlen($originalExt));
            $attemptCount = 2;
            
            while (file_exists($targetFile)) {
                $targetFile = $originalNoExt.'('.$attemptCount.').'.$originalExt;
                $attemptCount++;
            }
        }

        move_uploaded_file($_FILES['file']['tmp_name'], $targetFile);
        echo 'Success';
    } 
}
?>
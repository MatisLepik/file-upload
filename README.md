### File upload ###

A simple website that allows you to upload files - if you have the password - and allows anyone to see a gallery of the files that have already been uploaded. I originally made this for someone, but ended up using it for myself instead.

**You can see a [live version](http://matislepik.eu/files/) on my website.**

### Contact ###

- Matis Lepik

- matis.lepik@gmail.com

- www.matislepik.eu
/*jslint browser: true*/
/*global $, jQuery, console, FormData, alert*/
var fadeTime = 250;
var tableData;

function getTableHeight() {
    $('.files').hide();
    $('.files').css("max-height", $(document).height() - ($('.fileheader').height() + 285));
    $('.files').show();
}

function showUpload() {
    $('.uploadscreen').css('display', 'block');
    $('.uploadscreen').animate({
        opacity: 1
    }, fadeTime);
}

function hideUpload() {
    $('.uploadscreen').animate({
        opacity: 0
    }, fadeTime, function () {
        $('.uploadscreen').css('display', 'none');
        $('#pwBox').css('border-color', 'rgb(25,25,27)');
    });
}


function populateTable() {
    $.get("php/getfiles.php", function (data) {
        tableData = JSON.parse(data);
        
        for (var key in tableData) {
            var row = tableData[key];
            var readableSize = getReadableFileSize(row.size);
            
            $('#tableContent').append('<tr>' +
                '<td title="Preview in browser"><a href="' + row.url + '">' + row.name + '</a></td>' +
                '<td title="Last modified">' + row.modified + '</td>' +
                '<td title="File size"><span id="bytedata">' + row.size + '</span><span>' + readableSize + '</span></td>' +
                '<td title="Download link"><a class="downloadbutton clickable" href="download.php?file=' + row.name + '"></a></td>' +
                '</tr>');
        }
        $("table").trigger("update");
        $('th:contains("Last modified")').click();
        $('th:contains("Last modified")').click();
    });
}

function rePopulateTable() {
    $('#tableContent').empty();
    populateTable();
}
$(document).ready(function () {
    $('#jswarning').remove();
    $('.fileheader').show();
    $('#tableContent').empty();
    $('table').tablesorter({
        cssHeader: 'sortnone',
        headers: {
            1: {
                sorter: 'shortDate'
            },
            2: {
                sorter: 'digit',
                textExtraction: function (node) {
                    return node.childNodes[0].innerHTML;
                }
            },
            3: {
                sorter: false
            }
        },
        dateFormat: "mm/dd/yyyy"
    });

    populateTable();
    getTableHeight();

    $(window).on('resize', function () {
        getTableHeight();
    });

    $('#fileDialog').on('change', function () {
        var fileName = $('#fileDialog').val();
        $('#fileName').html(fileName.substr(fileName.lastIndexOf('\\') + 1));
        $('.uploadReady').show(200);
    });

    $('.uploadbutton').on('click', function () {
        showUpload();
    });
    
    $('.uploadscreen').on('click', function (evt) {
        hideUpload();
    });
    
    $(document).on('keydown', function (evt) {
        if (evt.which === 27) {
            hideUpload();
        }
    });
    
    $('.uploadbox').on('click', function (evt) {
        evt.stopPropagation();
    });
    
    $('.exitbutton').on('click', function () {
        hideUpload();
    });
    
    $('input[value="Upload"]').on('click', function (evt) {
        evt.preventDefault();
        $('#submitData').attr("Value", "Uploading...");

        var fileData = $('#fileDialog').prop('files')[0],
            formData = new FormData();
        formData.append('file', fileData);
        formData.append('pw', $('#pwBox').prop('value'));

        $.ajax({
            url: 'php/upload.php',
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            type: 'post',
            success: function (response) {
                console.log(response);
                switch (response) {
                case 'Success':
                    $('#pwBox').css('border-color', 'rgb(25,125,27)');
                    $('.uploadReady').hide(200);
                    rePopulateTable();
                    break;
                case 'Error':
                    alert("Server error.");
                    break;
                case 'Wrongpass':
                    $('#pwBox').css('border-color', 'rgb(125,25,27)');
                    $('#pwBox').val('');
                    break;
                }
                $('#submitData').attr("Value", "Upload");
            }
        });
    });
});

function getReadableFileSize(fileSizeInBytes) {
    var i = -1;
    var byteUnits = [' kB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];
    do {
        fileSizeInBytes = fileSizeInBytes / 1024;
        i++;
    } while (fileSizeInBytes > 1024);

    return Math.max(fileSizeInBytes, 0.1).toFixed(1) + byteUnits[i];
}